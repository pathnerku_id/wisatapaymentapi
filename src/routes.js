const { addNoteHandler, getAllNotesHAndler, getNoteByIdHandler, editNoteByIdHandler, deleteNoteByIdHandler, addTransactionIdHandler } = require("./handler");

const routes = [
  {
    method: "POST",
    path: "/notes",
    handler: addNoteHandler,
  },
  {
    method: "GET",
    path: "/notes",
    handler: getAllNotesHAndler,
  },
  {
    method: "GET",
    path: "/notes/{id}",
    handler: getNoteByIdHandler,
  },
  {
    method: "PUT",
    path: "/notes/{id}",
    handler: editNoteByIdHandler,
  },
  {
    method: "DELETE",
    path: "/notes/{id}",
    handler: deleteNoteByIdHandler,
  },
  {
    method: "POST",
    path: "/trans/{id}",
    handler: addTransactionIdHandler,
  },
];

module.exports = routes;
